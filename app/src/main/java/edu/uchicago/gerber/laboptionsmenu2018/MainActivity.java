package edu.uchicago.gerber.laboptionsmenu2018;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements ButtonFragment.OnFragmentInteractionListener{


    SimpleFragment simpleFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
               // showSomeRandomFragment();
            }
        });


        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        simpleFragment = new SimpleFragment();

       // ft.add(R.id.fragContainer, FactoryFragment.newInstance("Gerber", "UChicago"), "edu.uchicago.gerber.laboptionsmenu2018.FactoryFragment");
        ft.add(R.id.fragContainerNorth, new ButtonFragment(), "edu.uchicago.gerber.laboptionsmenu2018.ButtonFragment");
        ft.add(R.id.fragContainerSouth, simpleFragment, "edu.uchicago.gerber.laboptionsmenu2018.SimpleFragment");



        ft.commit();


    }


//
//    private void showSomeRandomFragment(){
//        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        Fragment[] fragments = {
//                fragmentManager.findFragmentByTag("edu.uchicago.gerber.laboptionsmenu2018.SimpleFragment"),
//                fragmentManager.findFragmentByTag("edu.uchicago.gerber.laboptionsmenu2018.FactoryFragment"),
//                fragmentManager.findFragmentByTag("edu.uchicago.gerber.laboptionsmenu2018.ButtonFragment"),
//        };
//
//        Random random = new Random();
//        int nFragShow  = random.nextInt(fragments.length);
//
//        for (int nC = 0; nC < fragments.length; nC++) {
//            if (nC == nFragShow){
//                ft.show(fragments[nC]);
//                continue;
//            }
//            ft.hide(fragments[nC]);
//        }
//
//        ft.commit();
//
//
//    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(String message) {


        Random r = new Random();

        if (simpleFragment != null){
            simpleFragment.setBackground(r.nextInt(256), r.nextInt(256), r.nextInt(256), r.nextInt(256));
        }


//
//        Toast toast=Toast.makeText(this,message,Toast.LENGTH_LONG);
//        toast.show();

    }
}
