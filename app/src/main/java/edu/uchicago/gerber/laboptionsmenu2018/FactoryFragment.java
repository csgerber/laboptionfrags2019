package edu.uchicago.gerber.laboptionsmenu2018;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FactoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FactoryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TextView name, school;


    public FactoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param nameParam Parameter 1.
     * @param schoolParam Parameter 2.
     * @return A new instance of fragment FactoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FactoryFragment newInstance(String nameParam, String schoolParam) {
        FactoryFragment fragment = new FactoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, nameParam);
        args.putString(ARG_PARAM2, schoolParam);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_factory, container, false);

        name = root.findViewById(R.id.name);
        school = root.findViewById(R.id.school);

        name.setText(mParam1);
        school.setText(mParam2);

        return  root;


    }

}
