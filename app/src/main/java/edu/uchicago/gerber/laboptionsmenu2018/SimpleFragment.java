package edu.uchicago.gerber.laboptionsmenu2018;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public class SimpleFragment extends Fragment {


    private ViewGroup background;

    public SimpleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_simple, container, false);

        background = root.findViewById(R.id.background);

        return  root;
    }


    public void setBackground(int alpha, int red, int green, int blue){

        background.setBackgroundColor(Color.argb(alpha, red, green, blue));
    }


}
